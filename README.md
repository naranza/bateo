# Welcome to Naranza Bateo

PHP-based tool for automated testing, particularly for web applications.
The tool helps in writing and executing tests to ensure that code behaves as expected and helps in identifying bugs or issues in the application.

Naranza Bateo allows developers to write test cases in PHP, and it often integrates with other testing frameworks and tools to streamline the testing process.
It supports various testing methodologies, including unit testing and functional testing.

Naranza Bateo is distinct in that it doesn't rely on traditional assertion-based testing.
Instead, it leverages native PHP code to conduct tests.
This approach allows developers to write tests using regular PHP functions and logic,
which can be particularly useful for those who are more comfortable with PHP syntax
and don't want to learn a new assertion-based syntax.

By using native PHP code, Naranza Bateo enables a more flexible and straightforward way to test applications.
It can simplify the process for developers who are already familiar with PHP and prefer to use it directly for their testing needs.

[more info on bateo.naranza.org](https://bateo.naranza.org/)